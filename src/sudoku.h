#ifndef SUDOKU_H
#define SUDOKU_H

#include <utility>
#include <stack>
#include <map>
#include <set>

#define BOARD_DIM 9
#define BLOCK_DIM 3
#define EMPTY_CELL 72
typedef std::pair<int,int> Cell;

bool read_board(const char *filename, char *board);
bool solve(char *board);
void print_board(const char *boad);

#endif
