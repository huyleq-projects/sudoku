#include "sudoku.h"

#include <cstdio>
#include <cstdlib>

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>

bool read_board(const char *filename, char *board) {
    std::ifstream ifs(filename);
    if (!ifs.is_open()) {
        printf("cannot open file %s\n", filename);
        return false;
    }

    std::string line;
    int row = 0;
    while (std::getline(ifs, line) && row < BOARD_DIM) {
        if (line.size() < BOARD_DIM) {
            printf("input file has less than 9 columns\n");
            return false;
        }

        for (int col = 0; col < BOARD_DIM; col++) {
            int i = row*BOARD_DIM + col;
            board[i] = line[col] - 48;

            if (board[i] != EMPTY_CELL && (board[i] < 1 || board[i] > 9)) {
                printf("invalid input %c at row %d col %d\n", line[col], row, col);
                return false;
            }
        }
        row++;
    }
    ifs.close();

    if (row != BOARD_DIM) {
        printf("input file has less than 9 rows\n");
        return false;
    }

    return true;
}

bool preprocess_board(char *board, std::map<Cell, std::set<char>> &choice_map) {
    for (int row = 0; row < BOARD_DIM; row++) {
        for (int col = 0; col < BOARD_DIM; col++) {
            int k = row*BOARD_DIM + col;
            if (board[k] == EMPTY_CELL) {
                std::set<char> choices;
                for (char v = 1; v <= 9; v++) choices.insert(v);

                for (int i = 0; i < BOARD_DIM; i++) { // loop over cols at row
                    int j = row*BOARD_DIM + i;
                    if (board[j] != EMPTY_CELL) choices.erase(board[j]);
                }

                for (int i = 0; i < BOARD_DIM; i++) { // loop over rows at col
                    int j = i*BOARD_DIM + col;
                    if (board[j] != EMPTY_CELL) choices.erase(board[j]);
                }

                int p = row/BLOCK_DIM, q = col/BLOCK_DIM; // block indices
                for (int i = 0; i < BLOCK_DIM; i++) {
                    int r = p*BLOCK_DIM + i;
                    for (int j = 0; j < BLOCK_DIM; j++) {
                        int c = q*BLOCK_DIM + j, n = r*BOARD_DIM + c;
                        if (board[n] != EMPTY_CELL) choices.erase(board[n]);
                    }
                }

                if (choices.empty()) {
                    printf("there is no possible value at (%d, %d)\n", row, col);
                    return false;
                }
                else choice_map[std::make_pair(row, col)] = choices;
            }
        }
    }
    return true;
}

bool valid_3by3block(int p, int q, const char *board) {
    p *= BLOCK_DIM; q *= BLOCK_DIM;
    std::set<char> char_set;
    for (int i = 0; i < BLOCK_DIM; i++) {
        int row = p + i;
        for (int j = 0; j < BLOCK_DIM; j++) {
            int col = q + j;
            char_set.insert(board[row*BOARD_DIM + col]);
        }
    }
    return (char_set.size() == BOARD_DIM);
}

bool valid_blocks(const char *board) {
    return (valid_3by3block(0, 0, board) &&
            valid_3by3block(0, 1, board) &&
            valid_3by3block(0, 2, board) &&
            valid_3by3block(1, 0, board) &&
            valid_3by3block(1, 1, board) &&
            valid_3by3block(1, 2, board) &&
            valid_3by3block(2, 0, board) &&
            valid_3by3block(2, 1, board) &&
            valid_3by3block(2, 2, board));
}

bool valid_row(int row, const char *board) {
    std::set<char> char_set;
    for (int col = 0; col < BOARD_DIM; col++)
        char_set.insert(board[row*BOARD_DIM+col]);
    return (char_set.size() == BOARD_DIM);
}

bool valid_rows(const char *board) {
    return (valid_row(0, board) && valid_row(1, board) && valid_row(2, board) &&
            valid_row(3, board) && valid_row(4, board) && valid_row(5, board) &&
            valid_row(6, board) && valid_row(7, board) && valid_row(8, board));
}

bool valid_col(int col, const char *board) {
    std::set<char> char_set;
    for (int row = 0; row < BOARD_DIM; row++)
        char_set.insert(board[row*BOARD_DIM+col]);
    return (char_set.size() == BOARD_DIM);
}

bool valid_cols(const char *board) {
    return (valid_col(0, board) && valid_col(1, board) && valid_col(2, board) &&
            valid_col(3, board) && valid_col(4, board) && valid_col(5, board) &&
            valid_col(6, board) && valid_col(7, board) && valid_col(8, board));
}

bool valid_board(const char *board) {
    return (valid_blocks(board) && valid_rows(board) && valid_cols(board));
}

void print_board(const char *board) {
    for (int i = 0; i < BOARD_DIM; i++) {
        for (int j = 0; j < BOARD_DIM; j++) printf("%d ", board[i*BOARD_DIM+j]);
        printf("\n");
    }
    return;
}
size_t print_choices(const std::map<Cell, std::set<char>> &choice_map) {
    size_t count = 1;
    for (const auto &item: choice_map) {
        const Cell &cell = item.first;
        const std::set<char> &choices = item.second;
        if (!choices.empty()) count *= choices.size();
        printf("possible choice at cell (%d, %d): ", cell.first, cell.second);
        for (const auto x: choices) printf("%d ", x);
        printf("\n");
    }
    return count;
}

void unfill(int row, int col, char x, char *board,
            const std::vector<Cell> &filled,
            std::map<Cell, std::set<char>> &choice_map) {
    for (const auto &cell: filled) choice_map[cell].insert(x);
    board[row*BOARD_DIM+col] = EMPTY_CELL;
    return;
}

std::vector<Cell> fillin(int row, int col, char x, char *board,
                         std::map<Cell, std::set<char>> &choice_map) {
    board[row*BOARD_DIM+col] = x;
    std::vector<Cell> filled;

    // remove x from possible choices at other cells in the same row/col/block
    for (int i = 0; i < BOARD_DIM; i++) { // loop over cols at row
        int j = row*BOARD_DIM + i;
        if (i != col && board[j] == EMPTY_CELL) {
            Cell cell = std::make_pair(row, i);
            std::set<char> &choices = choice_map[cell];
            if (choices.find(x) != choices.end()) {
                choices.erase(x);
                filled.emplace_back(cell);
            }
        }
    }

    for (int i = 0; i < BOARD_DIM; i++) { // loop over rows at col
        int j = i*BOARD_DIM + col;
        if (i != row && board[j] == EMPTY_CELL) {
            Cell cell = std::make_pair(i, col);
            std::set<char> &choices = choice_map[cell];
            if (choices.find(x) != choices.end()) {
                choices.erase(x);
                filled.emplace_back(cell);
            }
        }
    }

    int p = row/BLOCK_DIM, q = col/BLOCK_DIM; // block indices
    for (int i = 0; i < BLOCK_DIM; i++) {
        int r = p*BLOCK_DIM + i;
        for (int j = 0; j < BLOCK_DIM; j++) {
            int c = q*BLOCK_DIM + j, n = r*BOARD_DIM+ c;
            if (r != row && c != col && board[n] == EMPTY_CELL) {
                Cell cell = std::make_pair(r, c);
                std::set<char> &choices = choice_map[cell];
                if (choices.find(x) != choices.end()) {
                    choices.erase(x);
                    filled.emplace_back(cell);
                }
            }
        }
    }
    return filled;
}

void clear_choices(std::map<Cell, std::set<char>> &choice_map) {
    std::vector<Cell> filled;
    for (const auto &item: choice_map) {
        if (item.second.size() == 0) filled.emplace_back(item.first);
    }
    for (const auto &cell: filled) choice_map.erase(cell);
    return;
}

void elimination(char *board,
                 std::map<Cell, std::set<char>> &choice_map,
                 std::vector<Cell> &filled) {
    for (const auto &item: choice_map) {
        int row = item.first.first, col = item.first.second;

        if (item.second.size() == 1) {
            fillin(row, col, *item.second.begin(), board, choice_map);
            filled.emplace_back(item.first);
            continue;
        }

        for (char x: item.second) {
            bool notelsewhere = true; // to check if x cannot be anywhere else

            for (int i = 0; i < BOARD_DIM && notelsewhere; i++) { // loop over cols at row
                int j = row*BOARD_DIM + i;
                if (i != col && board[j] == EMPTY_CELL) {
                    std::set<char> &choices = choice_map[std::make_pair(row, i)];
                    notelsewhere = (choices.find(x) == choices.end());
                }
            }

            if (notelsewhere) { // x cannot be anywhere else on row
                fillin(row, col, x, board, choice_map);
                filled.emplace_back(item.first);
                break;
            }

            for (int i = 0; i < BOARD_DIM && notelsewhere; i++) { // loop over rows at col
                int j = i*BOARD_DIM + col;
                if (i != row && board[j] == EMPTY_CELL) {
                    std::set<char> &choices = choice_map[std::make_pair(i, col)];
                    notelsewhere = (choices.find(x) == choices.end());
                }
            }

            if (notelsewhere) { // x cannot be anywhere else on col
                fillin(row, col, x, board, choice_map);
                filled.emplace_back(item.first);
                break;
            }

            int p = row/BLOCK_DIM, q = col/BLOCK_DIM; // block indices
            for (int i = 0; i < BLOCK_DIM && notelsewhere; i++) {
                int r = p*BLOCK_DIM + i;
                for (int j = 0; j < BLOCK_DIM && notelsewhere; j++) {
                    int c = q*BLOCK_DIM + j, n = r*BOARD_DIM+ c;
                    if (r != row && c != col && board[n] == EMPTY_CELL) {
                        std::set<char> &choices = choice_map[std::make_pair(r, c)];
                        notelsewhere = (choices.find(x) == choices.end());
                    }
                }
            }

            if (notelsewhere) { // x cannot be anywhere else in this subblock
                fillin(row, col, x, board, choice_map);
                filled.emplace_back(item.first);
                break;
            }
        }
    }
    return;
}

void presolve(char *board, std::map<Cell, std::set<char>> &choice_map) {
    while (!choice_map.empty()) {
        std::vector<Cell> filled;
        elimination(board, choice_map, filled);

        if (filled.empty()) break;
        for (const auto &cell: filled) choice_map.erase(cell);
        clear_choices(choice_map);
    }
    return;
}

bool conflict(const std::map<Cell, std::set<char>> &choice_map) {
    for (const auto &item: choice_map) {
        if (item.second.empty()) return true;
    }
    return false;
}

bool helper(char *board,
            std::stack<Cell> &empty_cells,
            std::map<Cell, std::set<char>> &choice_map) {
    if (!empty_cells.empty()) { // if there is still empty cells
        Cell cell = empty_cells.top();
        empty_cells.pop(); // pop the top cell to fill

        int row = cell.first, col = cell.second;
        const std::set<char> choices = choice_map.at(cell); // choices at that cell
        choice_map.erase(cell);

        for (char v: choices) {
            std::vector<Cell> filled = fillin(row, col, v, board, choice_map);
            if (!conflict(choice_map) && helper(board, empty_cells, choice_map))
                return true;
            else unfill(row, col, v, board, filled, choice_map);
        }

        empty_cells.push(cell);
        choice_map[cell] = choices;
        return false;
    }
    else if (valid_board(board)) return true;
    else return false;
}

void recursive_solve(char *board, std::map<Cell, std::set<char>> &choice_map) {
    std::stack<Cell> empty_cells;
    for (const auto &item: choice_map) empty_cells.push(item.first);
    printf("before recursive_solve, there are %lu empty cells left\n", empty_cells.size());
    size_t count = print_choices(choice_map);
    printf("%lu possible boards to check\n", count);
    helper(board, empty_cells, choice_map);
    return;
}

bool solve(char *board) {
    std::map<Cell, std::set<char>> choice_map;
    if (!preprocess_board(board, choice_map)) return false;

    printf("there are %lu empty cells initially\n", choice_map.size());
    print_board(board);
    print_choices(choice_map);
    presolve(board, choice_map);
    if (!choice_map.empty()) recursive_solve(board, choice_map);

    print_board(board);
    if (valid_board(board)) printf("valid\n");
    else printf("invalid\n");

    return true;
}
