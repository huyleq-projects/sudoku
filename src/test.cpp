#include "sudoku.h"

#include <cstdio>
#include <cstdlib>

#include <iostream>
#include <chrono>

#define BOARD_DIM 9

int main(int argc, char **argv){
    if (argc != 2) {
        printf("Please specify input file\n");
        return 0;
    }

    char *board = new char[BOARD_DIM*BOARD_DIM];

    auto t1 = std::chrono::high_resolution_clock::now();
    if (read_board(argv[1], board)) solve(board);
    auto t2 = std::chrono::high_resolution_clock::now();

    auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
    std::cout << "It took me " << time_span.count() << " seconds." << std::endl;

    delete []board;
    return 0;
}
