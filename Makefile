cc = g++ -std=c++11

S = ./src
O = ./obj
B = ./bin

all: $(B)/test.x

$(O)/%.o: $(S)/%.cpp
	$(cc) -c $< -o $@

$(B)/test.x: $(O)/test.o $(O)/sudoku.o
	$(cc) $^ -o $@

clean:
	rm -rf $(B)/*.x $(O)/*.o
